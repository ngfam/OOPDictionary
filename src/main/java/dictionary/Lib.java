package dictionary;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import io.github.cdimascio.dotenv.Dotenv;
import org.json.*;


public class Lib {
    final Voice voice = VoiceManager.getInstance().getVoice("kevin16");
    static final GoogleTranslator googleTranslator = new GoogleTranslator(
            Dotenv.load().get("DEPLOYMENT_URL")
    );

    Lib() {
        voice.allocate();
    }

    static public Lib getInstance() {
        return new Lib();
    }

    /**
     * Padding a word to the right.
     * @return Padded string.
     */
    public static String rightPadding(String word, int length) {
        return String.format("%-" + length + "." + length + "s", word);
    }

    /**
     * Show a single word to CommandLine with required format.
     */
    public static String showSingleWord(String id, String word_target, String word_explain) {
        return String.format("%s| %s| %s", rightPadding(id, 5), rightPadding(word_target, 15), word_explain);
    }

    /**
     * read words from commandline.
     */
    public static ArrayList<Word> readWordsFromCommandLine(Scanner in) {
        ArrayList<Word> words = new ArrayList<>();
        int n = in.nextInt();
        in.nextLine();
        for(int i = 0; i < n; ++i) {
            String english = in.nextLine();
            String vietnamese = in.nextLine();
            words.add(new Word(english, vietnamese));
        }
        return words;
    }

    /**
     * read words from file.
     */
    public static ArrayList<Word> readWordsFromFile() {
        ArrayList<Word> words = new ArrayList<>();
        try {
            File dictionaryFile = new File("dictionary.txt");
            Scanner fileReader = new Scanner(dictionaryFile);
            while(true) {
                String line;
                try {
                    line = fileReader.nextLine();
                } catch (NoSuchElementException e) {
                    break;
                }
                String[] inputs = line.split("\t");
                words.add(new Word(inputs[0], inputs[1]));
            }
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred");
        }
        return words;
    }

    public static boolean isPrefixOf(String prefix, String target) {
        if (target.length() < prefix.length()) return false;
        return prefix.equals(target.substring(0, prefix.length()));
    }

    public static void writeToFile(String output) {
        try {
            FileWriter fileWriter = new FileWriter("dictionaryOut.txt");
            fileWriter.write(output);
            fileWriter.close();
            System.out.println("Successfully wrote dictionary to file.");
        } catch (IOException e) {
            System.out.println("An error occurred :(");
        }
    }

    private static String readAllFromJSON() {
        String jsonString = "";
        try {
            return Files.readString(Path.of("dictionary_final.json"));
        } catch (IOException e) {
            System.out.println("An error occurred");
        }
        return "";
    }

    public static ArrayList<Word> readWordsFromJSON() {
        ArrayList<Word> wordArrayList = new ArrayList<>();
        String jsonString = readAllFromJSON();
        JSONObject obj = new JSONObject(jsonString);
        JSONArray wordList = obj.getJSONArray("all_words");

        for(int i = 0; i < wordList.length(); ++i) {
            JSONObject currentWordJson = wordList.getJSONObject(i);
            String word = currentWordJson.getString("word");
            String wordType = currentWordJson.getString("word_type");
            String pronunciation = currentWordJson.getString("pronounciation");
            ArrayList<String> wordUsages = new ArrayList<>();
            ArrayList<String> wordExplanations = new ArrayList<>();
            JSONArray usageJson = currentWordJson.getJSONArray("usages");
            JSONArray explanationJson = currentWordJson.getJSONArray("eplanations");
            for(int j = 0; j < usageJson.length(); ++j) {
                wordUsages.add(usageJson.getString(j));
            }
            for(int j = 0; j < explanationJson.length(); ++j) {
                wordExplanations.add(explanationJson.getString(j));
            }
            wordArrayList.add(new Word(word, pronunciation, wordType, wordExplanations, wordUsages));
        }
        return wordArrayList;
    }

    public static ArrayList<Word> filterWord(ArrayList<Word> inputWords, ArrayList<String> types) {
        if (types.contains("khác")) {
            return inputWords;
        }
        ArrayList<Word> filteredWords = new ArrayList<>();
        for(Word word: inputWords) {
            boolean hasSelectedType = false;
            for (String type: types) {
                if (word.getWordType().contains(type)) {
                    hasSelectedType = true;
                    break;
                }
            }
            if (hasSelectedType) {
                filteredWords.add(word);
            }
        }

        return filteredWords;
    }

    private void _speak(String text) {
        this.voice.speak(text);
    }

    public static void speak(String text) {
        Lib.getInstance()._speak(text);
    }
}
