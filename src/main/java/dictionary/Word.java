package dictionary;

import java.util.ArrayList;

public class Word {
    private int wordId;
    private String wordTarget;
    private String wordType;
    private String pronunciation;
    private final ArrayList<String> wordExplanations;
    private final ArrayList<String> wordUsages;

    public Word() {
        this.wordTarget = "";
        this.pronunciation = "";
        this.wordType = "";
        this.wordExplanations = new ArrayList<>();
        this.wordUsages = new ArrayList<>();
    }

    public Word(String wordTarget, String wordExplain) {
        this.wordTarget = wordTarget.toLowerCase();
        this.wordExplanations = new ArrayList<>();
        this.wordUsages = new ArrayList<>();
        this.wordExplanations.add(wordExplain);
        this.pronunciation = "";
        this.wordType = "";
    }

    public Word(String wordTarget, String wordPronunciation, String wordType, String wordExplain) {
        this.wordTarget = wordTarget.toLowerCase();
        this.wordExplanations = new ArrayList<>();
        this.wordUsages = new ArrayList<>();
        this.wordExplanations.add(wordExplain);
        this.pronunciation = wordPronunciation;
        this.wordType = wordType;
    }

    public Word(String wordTarget, String pronunciation, String wordType, ArrayList<String> wordExplanations, ArrayList<String> wordUsages) {
        this.wordTarget = wordTarget;
        this.pronunciation = pronunciation;
        this.wordType = wordType;
        this.wordExplanations = wordExplanations;
        this.wordUsages = wordUsages;
        if (this.wordExplanations.isEmpty()) this.wordExplanations.add("Explanation not found");
    }

    /**
     * return the first word explain.
     */
    public String getWordExplain() {
        return this.wordExplanations.get(0);
    }

    /**
     * return word_target.
     */
    public String getWordTarget() {
        return wordTarget;
    }

    /**
     * set word_target.
     */
    public void setWordTarget(String wordTarget) {
        this.wordTarget = wordTarget;
    }

    /**
     * Transform word to single string.
     */
    public String toString() {
        return this.getWordTarget();
    }

    /**
     * return pronunciation.
     */
    public String getPronunciation() {
        return pronunciation;
    }

    /**
     *
     * set pronunciation.
     */
    public void setPronunciation(String pronunciation) {
        this.pronunciation = pronunciation;
    }

    /**
     * return wordType.
     */
    public String getWordType() {
        return wordType;
    }

    /**
     * set wordType.
     */
    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public ArrayList<String> getWordExplanations() {
        return this.wordExplanations;
    }

    public ArrayList<String> getWordUsages() {
        return this.wordUsages;
    }

    public void addWordExplanation(String wordExplanation) {
        this.wordExplanations.add(wordExplanation);
    }

    public void addWordUsage(String wordUsage) {
        this.wordUsages.add(wordUsage);
    }

    public int getWordId() {
        return wordId;
    }

    public void setWordId(int wordId) {
        this.wordId = wordId;
    }
}
