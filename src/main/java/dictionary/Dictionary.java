package dictionary;
import java.util.ArrayList;
import java.util.HashMap;

public class Dictionary extends ArrayList<Word> {
    ArrayList<Boolean> isDeleted;
    HashMap<String, Integer> lookUpMap;
    HashMap<String, ArrayList<Integer>> prefixMap;

    public Dictionary() {
        this.isDeleted = new ArrayList<>();
        this.prefixMap = new HashMap<>();
        this.lookUpMap = new HashMap<>();
    }

    /**
     * Add a new word to dictionary.
     * @param word new word.
     */
    public void addWord(Word word) {
        this.add(word);
        word.setWordId(this.size() - 1);
        this.lookUpMap.put(word.getWordTarget(), this.size() - 1);
        this.isDeleted.add(Boolean.FALSE);
        String target = word.getWordTarget();
        for(int i = 1; i <= target.length(); ++i) {
            String prefix = target.substring(0, i);
            if (!prefixMap.containsKey(prefix)) {
                prefixMap.put(prefix, new ArrayList<>());
            }
            prefixMap.get(prefix).add(this.size() - 1);
        }
    }

    public void deleteWord(int id) {
        this.isDeleted.set(id, Boolean.TRUE);
    }

    /**
     * @return words show in fixed format.
     */
    public String showWords() {
        String result = Lib.showSingleWord("No", "English", "Vietnamese");
        for(int i = 0; i < size(); ++i) {
            Word currentWord = get(i);
            result = result.concat("\n").concat(
                    Lib.showSingleWord(
                            Integer.toString(i + 1), currentWord.getWordTarget(), currentWord.getWordExplain()
                    )
            );
        }
        return result;
    }

    /**
     * @return english word explanation.
     */
    public Word loopUp(String target) {
        Integer indexFound = lookUpMap.get(target);
        if (indexFound == null || this.isDeleted.get(indexFound)) {
            return null;
        }
        return this.get(indexFound);
    }

    public ArrayList<Word> search(String target) {
        ArrayList<Word> searchResult = new ArrayList<>();
        ArrayList<Integer> searchedList = this.prefixMap.get(target);
        if (searchedList == null) {
            return searchResult;
        }
        for(int id: searchedList) {
            if (this.isDeleted.get(id)) {
                continue;
            }
            searchResult.add(this.get(id));
        }
        return searchResult;
    }

    public String toString() {
        String result = "";
        for(int i = 0; i < this.size(); ++i) {
            if (this.isDeleted.get(i)) {
                continue;
            }
            Word word = this.get(i);
            result = result.concat(word.toString()).concat("\n");
        }
        return result;
    }

    public void removeWord(int id) {
        this.isDeleted.set(id, true);
    }
}
