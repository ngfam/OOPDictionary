package dictionary;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class DictionaryCommandline {
    private final DictionaryManagement dictionaryManagement;

    public DictionaryCommandline() {
        this.dictionaryManagement = new DictionaryManagement();
        commandLineApp();
    }

    private void commandLineApp() {
        Scanner in = new Scanner(System.in);
        while(true) {
            boolean stop = false;
            String command = in.nextLine();
            switch (command) {
                case "readFromCommandLine" -> {
                    this.dictionaryManagement.insertFromList(Lib.readWordsFromCommandLine(in));
                    System.out.println("Successfully read from command line.");
                }
                case "readFromFile" -> {
                    this.dictionaryManagement.insertFromList(Lib.readWordsFromFile());
                    System.out.println("Successfully read from file.");
                }
                case "writeToFile" -> {
                    String dictionaryToString = this.dictionaryManagement.getDictionaryString();
                    Lib.writeToFile(dictionaryToString);
                }
                case "showAllWords" -> {
                    this.dictionaryManagement.showAllWords();
                }
                case "lookUp" -> {
                    System.out.println("Your desired word is...");
                    String word = in.nextLine();
                    Word wordFound = dictionaryManagement.dictionaryLookUp(word);
                    if (wordFound == null) {
                        System.out.println("Sorry we dont have that word here :(");
                    } else {
                        System.out.println("Word explanation is: " + wordFound);
                    }
                }
                case "search" -> {
                    System.out.println("Your desired word is...");
                    String word = in.nextLine();
                    ArrayList<Word> results = dictionaryManagement.dictionarySearch(word);
                    if (results.isEmpty()) {
                        System.out.println("No relevant word found :(");
                    } else {
                        System.out.println("Word relevant words are...");
                        for(Word resultWord: results) {
                            System.out.println(resultWord.toString());
                        }
                    }
                }
                case "googleTrans" -> {
                    System.out.println("Enter your desired sentence below...");
                    String line = in.nextLine();
                    try {
                        System.out.println("Here is your translated sentence...");
                        System.out.println(Lib.googleTranslator.translate("en", "vi", line));
                    } catch (IOException e) {
                        System.out.println("An error occurred :(");
                    }
                }
                case "readWordsFromJSON" -> {
                    this.dictionaryManagement.insertFromList(Lib.readWordsFromJSON());
                    System.out.println("Done reading JSON");
                }
                case "exit" -> {
                    stop = true;
                    System.out.println("Good bye!");
                }
                default -> {
                    System.out.println("Your desired command not found. Try again.");
                    stop = true;
                }
            }
            if (stop) {
                break;
            }
        }
    }

    public static void main(String args[]) {
        DictionaryCommandline dapp = new DictionaryCommandline();
    }
}
