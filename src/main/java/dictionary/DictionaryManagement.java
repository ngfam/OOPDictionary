package dictionary;
import java.util.ArrayList;

public class DictionaryManagement {
    Dictionary dictionary;

    DictionaryManagement() {
        this.dictionary = new Dictionary();
    }

    /**
     * insert words from a list to dictionary.
     * @param words word list.
     */
    public void insertFromList(ArrayList<Word> words) {
        for(Word word: words) {
            this.dictionary.addWord(word);
        }
    }

    /**
     * Show words in dictionary to commandline.
     */
    public void showAllWords() {
        System.out.println(dictionary.showWords());
    }

    /**
     * Look up a word target in dictionary.
     * @param target target word.
     * @return target word's explanation.
     */
    Word dictionaryLookUp(String target) {
        return this.dictionary.loopUp(target);
    }

    ArrayList<Word> dictionarySearch(String target) {
        target = target.toLowerCase();
        return this.dictionary.search(target);
    }

    public String getDictionaryString() {
        return this.dictionary.toString();
    }

    public void removeWord(Word word) {
        this.dictionary.removeWord(word.getWordId());
    }

    public void addWord(Word word) {
        this.dictionary.addWord(word);
    }
}
