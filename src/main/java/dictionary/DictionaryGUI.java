package dictionary;


import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DictionaryGUI implements Initializable {
    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonEdit;

    @FXML
    private Button buttonPronounceText;

    @FXML
    private Button buttonPronounceWord;

    @FXML
    private Button buttonSearch;

    @FXML
    private Button buttonSelectAll;

    @FXML
    private Button buttonTranslateEn;

    @FXML
    private Button buttonTranslateVi;

    @FXML
    private Button buttonUnselectAll;

    @FXML
    private CheckBox checkBoxAdjective;

    @FXML
    private CheckBox checkBoxAdverb;

    @FXML
    private CheckBox checkBoxDeterminer;

    @FXML
    private CheckBox checkBoxInterjection;

    @FXML
    private CheckBox checkBoxNoun;

    @FXML
    private CheckBox checkBoxOther;

    @FXML
    private CheckBox checkBoxPreposition;

    @FXML
    private CheckBox checkBoxPronoun;

    @FXML
    private CheckBox checkBoxVerb;

    @FXML
    private CheckBox checkBoxConjuction;

    @FXML
    private TextField editExplanation;

    @FXML
    private TextField editPronunciation;

    @FXML
    private TextField editRawWord;

    @FXML
    private TextField editWordType;

    @FXML
    private Button filterButton;

    @FXML
    private TextField inputSearch;

    @FXML
    private ListView<String> listExplanation;

    @FXML
    private ListView<String> listUsages;

    @FXML
    private ListView<Word> listWordFound;

    @FXML
    private MenuItem menuAddWord;

    @FXML
    private MenuItem menuClose;

    @FXML
    private MenuItem menuSearch;

    @FXML
    private MenuItem menuTranslate;

    @FXML
    private MenuItem menuItemRemove;

    @FXML
    private MenuItem menuItemEdit;

    @FXML
    private MenuItem menuItemView;

    @FXML
    private AnchorPane paneEditWord;

    @FXML
    private SplitPane paneSearch;

    @FXML
    private AnchorPane paneSelectedWord;

    @FXML
    private SplitPane paneTranslate;

    @FXML
    private TextField textDocumentRaw;

    @FXML
    private TextArea textDocumentTranslated;

    @FXML
    private Text textPronunciation;

    @FXML
    private Text textWordSelected;

    @FXML
    private Text textWordType;

    enum ScreenType {
        SELECT_WORD,
        EDIT_WORD,
        TRANSLATE,
    }

    private ScreenType currentScreen;

    private Word selectedWord;

    private String choosingWord;

    private ArrayList<CheckBox> allCheckBoxes;

    private DictionaryManagement dictionaryManagement;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.groupCheckBoxes();
        this.checkAllCheckBoxes();
        this.textWordSelected.setText("");
        this.textPronunciation.setText("");
        this.textWordType.setText("");
        this.dictionaryManagement = new DictionaryManagement();
        this.dictionaryManagement.insertFromList(Lib.readWordsFromJSON());
        this.inputSearch.setOnKeyPressed(this::handleInputSearchKeyPressed);;
        this.setCurrentScreen(ScreenType.SELECT_WORD);
        this.textDocumentTranslated.setWrapText(true);
    }

    private void groupCheckBoxes() {
        this.allCheckBoxes = new ArrayList<>();
        this.allCheckBoxes.add(this.checkBoxAdjective);
        this.allCheckBoxes.add(this.checkBoxAdverb);
        this.allCheckBoxes.add(this.checkBoxNoun);
        this.allCheckBoxes.add(this.checkBoxDeterminer);
        this.allCheckBoxes.add(this.checkBoxOther);
        this.allCheckBoxes.add(this.checkBoxInterjection);
        this.allCheckBoxes.add(this.checkBoxPreposition);
        this.allCheckBoxes.add(this.checkBoxPronoun);
        this.allCheckBoxes.add(this.checkBoxVerb);
        this.allCheckBoxes.add(this.checkBoxConjuction);
    }

    private void checkAllCheckBoxes() {
        for(CheckBox checkBox: this.allCheckBoxes) {
            checkBox.setSelected(true);
        }
    }

    private void uncheckAllCheckboxes() {
        for(CheckBox checkBox: this.allCheckBoxes) {
            checkBox.setSelected(false);
        }
    }

    private void setCurrentScreen(ScreenType type) {
        switch(type) {
            case SELECT_WORD -> {
                this.paneTranslate.setVisible(false);
                this.paneSearch.setVisible(true);
                this.paneEditWord.setVisible(false);
                this.paneSelectedWord.setVisible(true);
            }
            case EDIT_WORD -> {
                this.paneTranslate.setVisible(false);
                this.paneSearch.setVisible(true);
                this.paneSelectedWord.setVisible(false);
                this.paneEditWord.setVisible(true);
            }
            case TRANSLATE -> {
                this.paneSearch.setVisible(false);
                this.paneTranslate.setVisible(true);
            }
        }
    }

    private void handleInputSearchKeyPressed(KeyEvent keyEvent) {
        KeyCode keyCode = keyEvent.getCode();
        if (keyCode == KeyCode.ENTER) {
            this.handleSearchWord(this.inputSearch.getText());
        } else if (keyCode.isLetterKey()) {
            this.handleSuggestWord(this.inputSearch.getText().concat(keyCode.toString().trim()));
        }
    }

    private void handleSuggestWord(String word) {
        ArrayList<Word> wordsFound = Lib.filterWord(
                this.dictionaryManagement.dictionarySearch(word),
                this.getSelectedTypes()
        );
        ObservableList<Word> items = FXCollections.observableArrayList(wordsFound);
        this.listWordFound.setItems(items);
    }

    private void handleSearchWord(String word) {
        this.handleSuggestWord(word);
        Word wordFound = this.dictionaryManagement.dictionaryLookUp(word);
        if (wordFound != null) {
            this.setSelectedWord(wordFound);
        } else {
            ArrayList<Word> wordsFound = Lib.filterWord(
                    this.dictionaryManagement.dictionarySearch(word),
                    this.getSelectedTypes()
            );
            if (wordsFound.size() > 0) {
                this.setSelectedWord(wordsFound.get(0));
            }
        }
    }

    private void setSelectedWord(Word selectedWord) {
        this.selectedWord = selectedWord;
        this.inputSearch.setText(selectedWord.toString());
        this.textWordSelected.setText(selectedWord.getWordTarget());
        this.textPronunciation.setText(selectedWord.getPronunciation());
        this.textWordType.setText(selectedWord.getWordType());
        this.listUsages.setItems(
                FXCollections.observableArrayList(selectedWord.getWordUsages())
        );
        this.listExplanation.setItems(
                FXCollections.observableArrayList(selectedWord.getWordExplanations())
        );
    }

    private ArrayList<String> getSelectedTypes() {
        ArrayList<String> types = new ArrayList<>();
        for(CheckBox checkBox: this.allCheckBoxes) {
            if (checkBox.isSelected()) {
                types.add(checkBox.getText().toLowerCase());
            }
        }
        return types;
    }

    private boolean checkEditInformationSufficient() {
        if (this.editRawWord.getText().isEmpty() || this.editPronunciation.getText().isEmpty() ||
                this.editWordType.getText().isEmpty() || this.editExplanation.getText().isEmpty()) {
            Alert newAlert = new Alert(Alert.AlertType.INFORMATION);
            newAlert.setTitle("Thiếu thông tin");
            newAlert.setContentText("Vui lòng điền đầy đủ thông tin trước khi thêm hoặc sửa từ.");
            newAlert.show();
            return false;
        }
        return true;
    }

    @FXML
    void handleButtonEditWord(ActionEvent event) {
        if (!checkEditInformationSufficient()) {
            return;
        }

        Word currentWord = this.dictionaryManagement.dictionaryLookUp(choosingWord);
        this.dictionaryManagement.removeWord(currentWord);
        Word newWord = new Word(
                this.editRawWord.getText(),
                this.editPronunciation.getText(),
                this.editWordType.getText(),
                this.editExplanation.getText()
        );
        this.dictionaryManagement.addWord(newWord);
        this.handleSearchWord(newWord.getWordTarget());
    }

    @FXML
    void handleButtonAddWord(ActionEvent event) {
        if (!checkEditInformationSufficient()) {
            return;
        }
        Word newWord = new Word(
                this.editRawWord.getText(),
                this.editPronunciation.getText(),
                this.editWordType.getText(),
                this.editExplanation.getText()
        );
        this.dictionaryManagement.addWord(newWord);
        this.handleSearchWord(newWord.getWordTarget());
    }

    @FXML
    void handleButtonPronounceText(ActionEvent event) {
        Lib.speak(this.textDocumentTranslated.getText());
    }

    @FXML
    void handleButtonPronounceWord(ActionEvent event) {
        if (this.selectedWord != null) {
            Lib.speak(this.selectedWord.getWordTarget());
        }
    }

    @FXML
    void handleButtonSearch(ActionEvent event) {
        this.handleSearchWord(this.inputSearch.getText());
    }

    @FXML
    void handleButtonSelectAll(ActionEvent event) {
        this.checkAllCheckBoxes();
    }

    @FXML
    void handleButtonTranslateEn(ActionEvent event) throws Exception {
        this.textDocumentTranslated.setText(
            Lib.googleTranslator.translate("en", "vi", this.textDocumentRaw.getText())
        );
    }

    @FXML
    void handleButtonTranslateVi(ActionEvent event) throws Exception {
        this.textDocumentTranslated.setText(
                Lib.googleTranslator.translate("vi", "en", this.textDocumentRaw.getText())
        );
    }

    @FXML
    void handleButtonUnselectAll(ActionEvent event) {
        this.uncheckAllCheckboxes();
    }

    @FXML
    void handleCheckBoxAdjective(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxAdverb(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxConjuction(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxDeterminer(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxInterjection(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxNoun(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxOther(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxPreposition(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxPronoun(ActionEvent event) {

    }

    @FXML
    void handleCheckBoxVerb(ActionEvent event) {

    }

    @FXML
    void handleFilterButton(ActionEvent event) {
        this.handleSearchWord(this.inputSearch.getText());
    }

    @FXML
    void handleInputSearchChanged(InputMethodEvent event) {
        this.handleSearchWord(this.inputSearch.getText());
    }

    @FXML
    void handleMenuAddWord(ActionEvent event) {
        this.editRawWord.setText("");
        this.editPronunciation.setText("");
        this.editWordType.setText("");
        this.editExplanation.setText("");
        this.setCurrentScreen(ScreenType.EDIT_WORD);
    }

    @FXML
    void handleMenuCloseClick(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void handleMenuSearchClick(ActionEvent event) {
        this.setCurrentScreen(ScreenType.SELECT_WORD);
    }

    @FXML
    void handleMenuTranslateClick(ActionEvent event) {
        this.setCurrentScreen(ScreenType.TRANSLATE);
    }

    @FXML
    void handleWordClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.SECONDARY) {
            this.choosingWord = this.listWordFound.getSelectionModel().getSelectedItem().getWordTarget();
        }
    }

    @FXML
    void handleMenuItemView(ActionEvent event) {
        this.setCurrentScreen(ScreenType.SELECT_WORD);
        this.setSelectedWord(dictionaryManagement.dictionaryLookUp(choosingWord));
    }

    @FXML
    void handleMenuItemEdit(ActionEvent event) {
        this.setCurrentScreen(ScreenType.EDIT_WORD);
        if (!choosingWord.isEmpty()) {
            this.handleSearchWord(this.choosingWord);
            this.editRawWord.setText(this.selectedWord.getWordTarget());
            this.editExplanation.setText(this.selectedWord.getWordExplain());
            this.editPronunciation.setText(this.selectedWord.getPronunciation());
            this.editWordType.setText(this.selectedWord.getWordType());
        }
    }

    @FXML
    void handleMenuItemRemove(ActionEvent event) {
        Word toDelete = this.dictionaryManagement.dictionaryLookUp(choosingWord);
        dictionaryManagement.removeWord(toDelete);
        this.handleSearchWord(this.inputSearch.getText());
    }

}
